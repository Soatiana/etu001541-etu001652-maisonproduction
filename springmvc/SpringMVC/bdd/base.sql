create database film;
create role film login password 'film';
alter database film owner to film;

-- \q
-- psql -U film film
-- film

CREATE SEQUENCE "public".auteur_id_seq START WITH 1 INCREMENT BY 1;

CREATE SEQUENCE "public".detailscene_id_seq START WITH 1 INCREMENT BY 1;

CREATE SEQUENCE "public".genre_id_seq START WITH 1 INCREMENT BY 1;

CREATE SEQUENCE "public".personnage_id_seq START WITH 1 INCREMENT BY 1;

CREATE SEQUENCE "public".plateau_id_seq START WITH 1 INCREMENT BY 1;

CREATE SEQUENCE "public".plateauindisponible_id_seq START WITH 1 INCREMENT BY 1;

CREATE SEQUENCE "public".reaction_id_seq START WITH 1 INCREMENT BY 1;

CREATE SEQUENCE "public".scene_id_seq START WITH 1 INCREMENT BY 1;

CREATE SEQUENCE "public".unite_id_seq START WITH 1 INCREMENT BY 1;

CREATE  TABLE "public".auteur ( 
	id                   integer DEFAULT nextval('auteur_id_seq'::regclass) NOT NULL  ,
	nomauteur            varchar(255)    ,
	mdp                  varchar(100)    ,
	CONSTRAINT auteur_pkey PRIMARY KEY ( id )
 );

CREATE  TABLE "public".genre ( 
	id                   integer DEFAULT nextval('genre_id_seq'::regclass) NOT NULL  ,
	nomgenre             varchar(255)    ,
	CONSTRAINT genre_pkey PRIMARY KEY ( id )
 );

CREATE  TABLE "public".personnage ( 
	id                   integer DEFAULT nextval('personnage_id_seq'::regclass) NOT NULL  ,
	nompersonnage        varchar(255)    ,
	idgenre              integer    ,
	datenaissance        date    ,
	CONSTRAINT personnage_pkey PRIMARY KEY ( id )
 );

CREATE  TABLE "public".plateau ( 
	id                   integer DEFAULT nextval('plateau_id_seq'::regclass) NOT NULL  ,
	nomplateau           varchar(255)    ,
	descri               varchar(2555)    ,
	etat                 integer DEFAULT 0   ,
	x                    double precision    ,
	y                    double precision    ,
	CONSTRAINT plateau_pkey PRIMARY KEY ( id )
 );

CREATE  TABLE "public".plateauindisponible ( 
	id                   integer DEFAULT nextval('plateauindisponible_id_seq'::regclass) NOT NULL  ,
	idplateau            integer    ,
	"date"               date DEFAULT CURRENT_DATE   ,
	observation          varchar DEFAULT 'Planifie pour tournage'::character varying   ,
	CONSTRAINT pk_plateauindisponible PRIMARY KEY ( id )
 );

CREATE  TABLE "public".reaction ( 
	id                   integer DEFAULT nextval('reaction_id_seq'::regclass) NOT NULL  ,
	nomreaction          varchar(255)    ,
	CONSTRAINT reaction_pkey PRIMARY KEY ( id )
 );

CREATE  TABLE "public".scene ( 
	id                   integer DEFAULT nextval('scene_id_seq'::regclass) NOT NULL  ,
	titre                varchar(255)    ,
	idauteur             integer DEFAULT 1   ,
	idplateau            integer    ,
	duree                double precision DEFAULT 0   ,
	status               integer DEFAULT 3   ,
	CONSTRAINT scene_pkey PRIMARY KEY ( id )
 );

CREATE  TABLE "public".unite ( 
	id                   integer DEFAULT nextval('unite_id_seq'::regclass) NOT NULL  ,
	nomunite             varchar(100)    ,
	CONSTRAINT unite_pkey PRIMARY KEY ( id )
 );

CREATE  TABLE "public".detailscene ( 
	id                   integer DEFAULT nextval('detailscene_id_seq'::regclass) NOT NULL  ,
	idscene              integer    ,
	idpersonnage         integer    ,
	idreaction           integer    ,
	"action"             varchar(255)    ,
	duree                double precision DEFAULT 0.5   ,
	heuredebut           timestamp DEFAULT CURRENT_TIMESTAMP   ,
	heurefin             timestamp DEFAULT CURRENT_TIMESTAMP   ,
	CONSTRAINT detailscene_pkey PRIMARY KEY ( id )
 );

ALTER TABLE "public".detailscene ADD CONSTRAINT detailscene_idpersonnage_fkey1 FOREIGN KEY ( idpersonnage ) REFERENCES "public".personnage( id );

ALTER TABLE "public".detailscene ADD CONSTRAINT detailscene_idreaction_fkey1 FOREIGN KEY ( idreaction ) REFERENCES "public".reaction( id );

ALTER TABLE "public".detailscene ADD CONSTRAINT detailscene_idscene_fkey1 FOREIGN KEY ( idscene ) REFERENCES "public".scene( id );

ALTER TABLE "public".personnage ADD CONSTRAINT personnage_idgenre_fkey1 FOREIGN KEY ( idgenre ) REFERENCES "public".genre( id );

ALTER TABLE "public".plateauindisponible ADD CONSTRAINT fk_plateauindisponible_plateau FOREIGN KEY ( idplateau ) REFERENCES "public".plateau( id );

ALTER TABLE "public".scene ADD CONSTRAINT scene_idauteur_fkey1 FOREIGN KEY ( idauteur ) REFERENCES "public".auteur( id );

ALTER TABLE "public".scene ADD CONSTRAINT scene_idplateau_fkey1 FOREIGN KEY ( idplateau ) REFERENCES "public".plateau( id );
