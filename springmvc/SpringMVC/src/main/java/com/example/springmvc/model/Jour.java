package com.example.springmvc.model;

import java.sql.Date;
import java.util.List;

public class Jour {
    private int nbJour;
    private List<Scene> listScene;

    private Date date;

    public int getNbJour() {
        return nbJour;
    }

    public void setNbJour(int nbJour) {
        this.nbJour = nbJour;
    }

    public List<Scene> getListScene() {
        return listScene;
    }

    public void setListScene(List<Scene> listScene) {
        this.listScene = listScene;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
