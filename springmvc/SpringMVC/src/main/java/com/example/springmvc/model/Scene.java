package com.example.springmvc.model;

import javax.persistence.*;

@Entity
@Table(name = "scene")
public class Scene {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "titre")
    private String titre;

    @Column(name="idauteur",insertable = false) // Mbola hiova
    private Integer idauteur;

//    @Column(name="idplateau")
//    private Integer idplateau;

    @ManyToOne
    @JoinColumn(name = "idplateau")
    private Plateau plateau;

    @Column(name="duree")
    private double duree;

    @Column(name = "status", insertable = false)
    private Integer status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public Integer getIdauteur() {
        return idauteur;
    }

    public void setIdauteur(Integer idauteur) {
        this.idauteur = idauteur;
    }

//    public Integer getIdplateau() {
//        return idplateau;
//    }
//
//    public void setIdplateau(Integer idplateau) {
//        this.idplateau = idplateau;
//    }

    public double getDuree() {
        return duree;
    }

    public void setDuree(double duree) {
        this.duree = duree;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Plateau getPlateau() {
        return plateau;
    }

    public void setPlateau(Plateau plateau) {
        this.plateau = plateau;
    }
}
