package com.example.springmvc.service;

import com.example.springmvc.dao.HibernateDAO;
import com.example.springmvc.model.Reaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReactionService {
    @Autowired
    HibernateDAO hibernateDAO;

//    get all reaction
    public List<Reaction> getAllReaction(){
        return hibernateDAO.getAll(Reaction.class);
    }
}
