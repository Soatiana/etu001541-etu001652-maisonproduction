package com.example.springmvc.service;

import com.example.springmvc.dao.HibernateDAO;
import com.example.springmvc.model.DetailScene;
import com.example.springmvc.model.Jour;
import com.example.springmvc.model.Plateau;
import com.example.springmvc.model.Scene;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.sql.Date;
import java.util.*;
import java.util.logging.Logger;

@Service
public class SceneService {
    @Autowired
    HibernateDAO hibernateDAO;

    @Autowired
    PlateauService plateauService;

    @Autowired
    DetailSceneService detailSceneService;

    //    Get all scene paginé
    public List<Scene> getAllScene(Integer offset, Integer limit){
        return hibernateDAO.getAllByPagination(Scene.class, offset, limit);
    }

//    Save scene
    public void saveScene(Scene scene) throws Exception {
        hibernateDAO.save(scene);
    }

//    Get last idScene
    public Integer getLastId(){
        return hibernateDAO.getLastIdScene();
    }

//    Get by id
    public Scene getById(Integer id){
        return hibernateDAO.getSceneById(id);
    }

    // Planifier des scenes
    public List<List<Scene>> planScenes(List<Scene> scenes) {
        List<List<Scene>> days = new ArrayList<>();
        List<Scene> currentDayScenes = new ArrayList<>();
        double currentDayDuration = 0.0;
        int currentDayIndex = 0;

        for (Scene scene : scenes) {
            if (currentDayDuration + scene.getDuree() <= 8.0) {
                currentDayScenes.add(scene);
                currentDayDuration += scene.getDuree();
            } else {
                days.add(currentDayIndex, currentDayScenes);
                currentDayIndex++;
                currentDayScenes = new ArrayList<>();
                currentDayScenes.add(scene);
                currentDayDuration = scene.getDuree();
            }
        }

        if (!currentDayScenes.isEmpty()) {
            days.add(currentDayIndex, currentDayScenes);
        }

        return days;
    }

//    Get all scene
    public List<Scene> getAllScene(){
        return hibernateDAO.getAll(Scene.class);
    }

//    Calcul somme duree d'une scene
    public double getSommeDuree(Integer idscene){
        double somme = 0;
        List<DetailScene> liste = hibernateDAO.getAllBy(idscene);
        for (int i = 0; i < liste.size(); i++) {
            somme += liste.get(i).getDuree();
        }
        return somme;
    }

//   Update scene
    public void updateDureeScene(Integer id) throws Exception {
        hibernateDAO.updateDureeScene(id,this.getSommeDuree(id));
    }

//    Get scenes dont les plateaux sont disponibles
    public List<Scene> getAllLibres(){
        return hibernateDAO.getSceneWithPlateauLibre();
    }

//    Update status scene
    public void updateStatusScene(Integer id, Integer status) throws Exception {
        hibernateDAO.updateStatusScene(id,status);
    }

//    Get scenes where status = 0 et plateau disponibles
    public List<Scene> getScenesNonOccupees(Date d1, Date d2){
        return hibernateDAO.getScenesNonOccupees(d1, d2);
    }

    //    liste scene where idScene ne idScene n-checkena
    public List<Scene> listSceneById(int idScene){
        return hibernateDAO.findScenesById(idScene);
    }

    //    liste scene rehetra par rapport ane list idScene retra n-checkena
    public List<Scene> listeScene(List<Integer> listId) throws Exception {
        List<Scene> valiny = new ArrayList<>();
        List<Scene> listScene = null;
        for (int i = 0; i < listId.size(); i++) {
            listScene = this.listSceneById(listId.get(i));
            for (int j = 0; j < listScene.size(); j++) {
                valiny.add(listScene.get(j));
            }
        }
        return valiny;
    }

    //    liste scene par rapport idPlateau
    public List<Scene> listeSceneByIdPlateau(List<Scene> listScene, int idPlateau) {
        List<Scene> valiny = new ArrayList<>();
        for (int j = 0; j < listScene.size(); j++) {
            if (listScene.get(j).getPlateau().getId() == idPlateau) {
                valiny.add(listScene.get(j));
            } else {
                System.out.println("next");
            }
        }
        return valiny;
    }


//    Difference entre 2 dates
    public int differencedate(Date date1, Date date2){
        long millis1 = date1.getTime();
        long millis2 = date2.getTime();
        long differenceMillis = millis2 - millis1;
        int differenceEnJour = (int) (differenceMillis/ (24*60*60*1000));
        return differenceEnJour;
    }

//    Liste de chaque jour dans une intervalle de date donnée
    public ArrayList<Date> listDate(Date dateDebut, Date dateFin) {
        ArrayList<Date> list = new ArrayList<>();
        for (java.sql.Date date = dateDebut; date.compareTo(dateFin) <= 0;) {
            list.add(date);
            Calendar c = Calendar.getInstance();
            c.setTime(date);
            c.add(Calendar.DATE, 1);
            date = new java.sql.Date(c.getTimeInMillis());
        }
        return list;
    }

//    Voir si ine date est ferie ou pas
    public boolean isFerier(Date daty) {
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(daty);
        // Récupérer l'année, le mois et le jour
        int year = cal.get(GregorianCalendar.YEAR);
        int month = cal.get(GregorianCalendar.MONTH);
        int day = cal.get(GregorianCalendar.DAY_OF_MONTH);
        int week = cal.get(GregorianCalendar.DAY_OF_WEEK);
        System.out.println("week "+week);
        if (month == GregorianCalendar.JANUARY && day == 1) {
            // Jour de l'an
            return true;
        } else if (month == GregorianCalendar.MAY && day == 1) {
            // Fête du travail
            return true;
        } else if (month == GregorianCalendar.JUNE && day == 26) {
            // Fête nationale
            return true;
        } else if (month == GregorianCalendar.AUGUST && day == 15) {
            // Assomption
            return true;
        } else if (month == GregorianCalendar.NOVEMBER && day == 1) {
            // Toussaint
            return true;
        } else if (month == GregorianCalendar.DECEMBER && day == 25) {
            // Noël
            return true;
        }else if(month == GregorianCalendar.DECEMBER && day == 31){
            // Fin annee
            return true;
        }else if (week == 1){
            return true;
        }
        else {
            return false;
        }
    }

//    Liste des dates valables (non ferie et non dimanche)
    public ArrayList<Date> listDateNotFerier(Date dateDebut, Date dateFin){
        ArrayList<Date> listNonferier = new ArrayList<>();
        ArrayList<Date> listInitiale = this.listDate(dateDebut, dateFin);
        boolean isFerier = false;
        for(int i=0;i<listInitiale.size();i++){
            isFerier = this.isFerier(listInitiale.get(i));
            if(isFerier==false){
                listNonferier.add(listInitiale.get(i));
            }
        }
        return listNonferier;
    }

//    planning
    public List<Jour> planifier(int jourTravaille, List<Integer> listId, ArrayList<Date> listeDate) throws Exception {
        List<Jour> listJour = new ArrayList<>();
        List<Scene> listScene = null;
        List<Scene> listSceneEnsemble = this.listeScene(listId);
        List<Plateau> listeP = plateauService.trierParDistance(plateauService.listePlateau(plateauService.plateauDistinct(listId)));

        List<Jour> valiny = new ArrayList<>();

        for (int i = 0; i < listeP.size(); i++) {
            listScene = this.listeSceneByIdPlateau(listSceneEnsemble, listeP.get(i).getId());
            while (listScene.size() > 0) {
                int somme = 0;
                ArrayList<Scene> listSceneJour = new ArrayList<>();
                for (int k = 0; k < listScene.size(); k++) {
                    if (somme + listScene.get(k).getDuree() <= jourTravaille) {
                        somme += listScene.get(k).getDuree();
                        listSceneJour.add(listScene.get(k));
                    }
                }
                if (listSceneJour.size() > 0) {
                    Jour jour = new Jour();
                    jour.setNbJour(listJour.size() + 1);
                    jour.setListScene(listSceneJour);
                    jour.setDate(listeDate.get(listJour.size()));
                    listJour.add(jour);
                    listScene.removeAll(listSceneJour);
                } else {
                    break;
                }
            }
        }
        int s = 0;
        for (int l = 0; l < listJour.size(); l++) {
            System.out.println("Somme jours : " + s);
            System.out.println("NBJOUR : "+listeDate.size());
            if (s < listeDate.size()) {
                listJour.get(l).setDate(listeDate.get(l));
            }
            valiny.add(listJour.get(l));
            s += 1;
        }
        return valiny;
    }

//    Maka scenes by status pour recherche
    public List<Scene> findScenesByStatus(Integer status){
        return hibernateDAO.findScenesByStatus(status);
    }

//    supprimer une scene dans une liste de scene
    public void supprimerScene(List<Scene> scenes, int idSceneToDelete) {
        for (Iterator<Scene> iterator = scenes.iterator(); iterator.hasNext();) {
            Scene scene = iterator.next();
            if (scene.getId() == idSceneToDelete) {
                iterator.remove();
            }
        }
    }

//    supprimer un Jour dans une List<Jour> si il n'y a plus de scene dans sa listeScene
    public void supprimerJour(List<Jour> jours) {
        for (Iterator<Jour> iterator = jours.iterator(); iterator.hasNext();) {
            Jour jour = iterator.next();
            if (jour.getListScene().isEmpty()) {
                iterator.remove();
            }
        }
    }

//    generer pdf a partir d'un planning donné
public void generer(String nomPdf, List<Jour> listJour, List<Integer> listeId) throws DocumentException, FileNotFoundException {
    Document document = new Document();

    try {
        PdfWriter.getInstance(document, new FileOutputStream("D:\\ITu\\S6\\Mr Naina Framework\\Film git\\etu001541-etu001652-maisonproduction\\springmvc\\SpringMVC\\pdf\\" + nomPdf + ".pdf"));
    } catch (FileNotFoundException ex) {
        throw ex;
    }

    document.open();
    document.add(new Paragraph("                                                             PLANNING"));

    for (Jour jour : listJour) {
        document.add(new Paragraph("Jour " + jour.getNbJour() + " : " + jour.getDate()));
        for (Scene scene : jour.getListScene()) {
            document.add(new Paragraph("      -" + scene.getTitre() + " (Plateau : " + scene.getPlateau().getNomplateau() + " " + scene.getPlateau().getDescri() + ")"));

            for (Integer id : listeId) {
                List<DetailScene> details = detailSceneService.getAllBy(id);
                for (DetailScene detail : details) {
                    if (detail.getScene().getId() == scene.getId()) {
                        document.add(new Paragraph("                ." + detail.getPersonnage().getNompersonnage() + " (" + detail.getReaction().getNomreaction() + ") : " + detail.getAction()));
                    }
                }
            }
            document.add(new Paragraph("\n"));
        }
    }

    document.close();
}

}
