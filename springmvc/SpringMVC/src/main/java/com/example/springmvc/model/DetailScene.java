package com.example.springmvc.model;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name="detailscene")
public class DetailScene {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

//    @Column(name="idscene")
//    private Integer idscene;

//    @Column(name="idpersonnage")
//    private Integer idpersonnage;
//
//    @Column(name="idreaction")
//    private Integer idreaction;
    @ManyToOne
    @JoinColumn(name = "idscene")
    private Scene scene;

    @ManyToOne
    @JoinColumn(name = "idpersonnage")
    private Personnage personnage;

    @ManyToOne
    @JoinColumn(name = "idreaction")
    private Reaction reaction;
    @Column(name="action")
    private String action;

    @Column(name="duree") // mety mbola hiova
    private double duree;
    @Column(name = "heuredebut", insertable = false) // mbola hiova
    private Timestamp heuredebut;

    @Column(name = "heurefin", insertable = false) // mbola hiova
    private Timestamp heurefin;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }



//    public Integer getIdscene() {
//        return idscene;
//    }
//
//    public void setIdscene(Integer idscene) {
//        this.idscene = idscene;
//    }
//
//    public Integer getIdpersonnage() {
//        return idpersonnage;
//    }
//
//    public void setIdpersonnage(Integer idpersonnage) {
//        this.idpersonnage = idpersonnage;
//    }
//
//    public Integer getIdreaction() {
//        return idreaction;
//    }
//
//    public void setIdreaction(Integer idreaction) {
//        this.idreaction = idreaction;
//    }


    public Scene getScene() {
        return scene;
    }

    public void setScene(Scene scene) {
        this.scene = scene;
    }

    public Personnage getPersonnage() {
        return personnage;
    }

    public void setPersonnage(Personnage personnage) {
        this.personnage = personnage;
    }

    public Reaction getReaction() {
        return reaction;
    }

    public void setReaction(Reaction reaction) {
        this.reaction = reaction;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public double getDuree() {
        return duree;
    }

    public void setDuree(double duree) {
        this.duree = duree;
    }

    public Timestamp getHeuredebut() {
        return heuredebut;
    }

    public void setHeuredebut(Timestamp heuredebut) {
        this.heuredebut = heuredebut;
    }

    public Timestamp getHeurefin() {
        return heurefin;
    }

    public void setHeurefin(Timestamp heurefin) {
        this.heurefin = heurefin;
    }
}
