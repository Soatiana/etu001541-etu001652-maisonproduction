package com.example.springmvc.controller;

import com.example.springmvc.model.*;
import com.example.springmvc.service.*;
import com.itextpdf.text.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.FileNotFoundException;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Controller
public class SceneController {
    @Autowired
    PlateauService plateauService;

    @Autowired
    SceneService sceneService;

    @Autowired
    PersonnageService personnageService;

    @Autowired
    ReactionService reactionService;

    @Autowired
    UniteService uniteService;

    @Autowired
    DetailSceneService detailSceneService;

    @Autowired
    PlateauIndisponibleService plateauIndisponibleService;

    List<Scene> listescene = new ArrayList<>();
    String message = null;

    List<Jour> listeJour = new ArrayList<>();

    @GetMapping("/insertionscene")
    public ModelAndView afficheInsertionScene(){
        ModelAndView m = new ModelAndView();
        m.addObject("listeplateau",plateauService.getAllPlateau());
        m.setViewName("ajoutscene");
        return m;
    }

    @GetMapping("/insertiondetails")
    public ModelAndView afficheInsertionDetailScene(Model model){
        ModelAndView m = new ModelAndView();
        m.addObject("listepersonnage",personnageService.getAllPersonnage());
        m.addObject("listereaction", reactionService.getAllReaction());
        m.addObject("listeunite", uniteService.getAllUnite());

        if(model.getAttribute("message") != null){
            m.addObject("message", model.getAttribute("message"));
        }

        m.setViewName("ajoutdetails");
        return m;
    }

    @PostMapping("/insererscene")
    public String insererScene(Model model, HttpServletRequest request, HttpSession session) throws Exception {
        Scene sce = new Scene();
        sce.setTitre(request.getParameter("titre"));
        Plateau p = new Plateau();
        p.setId(Integer.parseInt(request.getParameter("idplateau")));
        sce.setPlateau(p);
        sceneService.saveScene(sce);
        session.setAttribute("idscene", sceneService.getLastId());
        return "redirect:/insertiondetails";
    }

    @GetMapping("/insererdetails")
    public String insererDetailScene(Model model,HttpServletRequest request, HttpSession session) throws Exception {
        DetailScene sce = new DetailScene();

        double duree = Double.parseDouble(request.getParameter("duree"));
        Integer idunite = Integer.parseInt(request.getParameter("idunite"));

        double val = 0;
        if (idunite == 1){
            val = duree / 3600;
        }
        if(idunite ==2){
            val = duree / 60;
        }

        if (idunite == 3){
            val = duree;
        }

        if(duree == 4){
            val = duree * 8;
        }
        Personnage personnage = new Personnage();
        Reaction reaction = new Reaction();
        Scene scene= new Scene();
        personnage.setId(Integer.parseInt(request.getParameter("idpersonnage")));
        reaction.setId(Integer.parseInt(request.getParameter("idreaction")));
        scene.setId((Integer) session.getAttribute("idscene"));
        sce.setAction(request.getParameter("action"));
        sce.setDuree(val);
        sce.setPersonnage(personnage);
        sce.setReaction(reaction);
        sce.setScene(scene);
//        sce.setHeuredebut(Timestamp.valueOf(request.getParameter("heuredebut")));
//        sce.setHeurefin(Timestamp.valueOf(request.getParameter("heurefin")));
//        sce.setDuree(Double.parseDouble(request.getParameter("duree")));

        detailSceneService.save(sce);

        sceneService.updateDureeScene(sce.getScene().getId());

        model.addAttribute("message","Details ajoutes !");

        return "forward:/insertiondetails";
    }

//    @GetMapping("/listescene")
//    public ModelAndView listerScene(/*@RequestParam(name = "offset",defaultValue = "0") Integer offset,
//                                    @RequestParam(name="limit",defaultValue = "2") Integer limit*/){
//        ModelAndView m = new ModelAndView();
//
////        m.addObject("liste", sceneService.getAllScene(offset, limit));
////        m.addObject("offset", offset);
////        m.addObject("limit", limit);
//        //m.addObject("liste", sceneService.getAllScene()); // tsy mbola paginé
//        m.addObject("liste", sceneService.getAllLibres());
//        m.setViewName("listescene");
//        return m;
//    }

    @GetMapping("/plannifier")
    public String plannifier(Model model, HttpServletRequest request, HttpSession session) throws Exception {
        String[] checked = request.getParameterValues("valide");
        List<Jour> valiny = null;
        List<Integer> listeIdScene = new ArrayList<>();
        Date d1 = (Date) session.getAttribute("date1");
        Date d2 = (Date) session.getAttribute("date2");
        ArrayList<Date> listeDateValables = sceneService.listDateNotFerier(d1, d2);
        int tailleDates = listeDateValables.size();
        System.out.println("DATE 1 : "+d1);
        System.out.println("DATE 2 : "+d2);
        System.out.println("TAILLE DATE  : "+tailleDates);
        for (int i = 0; i < checked.length; i++) {
            listeIdScene.add(Integer.parseInt(checked[i]));
            System.out.println("id scene checkee : "+listeIdScene.get(i));
        }
        valiny = sceneService.planifier(8,listeIdScene, listeDateValables);

        model.addAttribute("liste", valiny);
        model.addAttribute("listeDate",listeDateValables);
        model.addAttribute("detailscene", detailSceneService);

        session.setAttribute("listeplanifie", valiny);
        return "planning";
    }

    @GetMapping("/periode")
    public String afficherInsertionPeriode(){
        return "periode";
    }

    @PostMapping("/proposer")
    public String proposerScenes(HttpSession session, HttpServletRequest request, Model model){
        Date d1 = Date.valueOf(request.getParameter("date1"));
        Date d2 = Date.valueOf(request.getParameter("date2"));
        session.setAttribute("date1", d1);
        session.setAttribute("date2", d2);

        List<Scene> scenes = sceneService.getScenesNonOccupees(d1,d2);
        model.addAttribute("liste", scenes);
        return "listescene";
    }

    @GetMapping("/modifplateau")
    public String afficherModifPlateau(Model model){
        List<Plateau> liste = plateauService.getAllPlateau();
        model.addAttribute("listeplateau", liste);
        return "modificationplateau";
    }

    @GetMapping("/modifierplateau")
    public String modifierPlateau(HttpServletRequest request, Model model) throws Exception {
        PlateauIndisponible plateauIndisponible = new PlateauIndisponible();
        Plateau plateau = new Plateau();
        plateau.setId(Integer.parseInt(request.getParameter("idplateau")));

        plateauIndisponible.setPlateau(plateau);
        plateauIndisponible.setDate(Date.valueOf(request.getParameter("date")));
        plateauIndisponible.setObservation(request.getParameter("observation"));

        plateauIndisponibleService.save(plateauIndisponible);
        model.addAttribute("ok", "Etat du plateau modifie ! Plateau desormais indisponible a cette date.");
        return "forward:/modifplateau";
    }


    @GetMapping("/confirmer")
    public String confirmer(Model model, HttpServletRequest request, HttpSession session) throws Exception {
        List<Jour> listeModifie = (List<Jour>) session.getAttribute("listeplanifie");
        int nbScenes = Integer.parseInt(request.getParameter("size"));
        String[] scenes = new String[nbScenes];

        PlateauIndisponible plateauIndisponible = new PlateauIndisponible();
        for (int i = 0; i < listeModifie.size(); i++) {
            plateauIndisponible.setDate(listeModifie.get(i).getDate());
            for (int j = 0; j < listeModifie.get(i).getListScene().size(); j++) {
                plateauIndisponible.setPlateau(listeModifie.get(i).getListScene().get(j).getPlateau());
                plateauIndisponible.setObservation("Plannifie pour tournage");
            }
            plateauIndisponibleService.save(plateauIndisponible);
        }

        for (int i = 0; i < nbScenes; i++) {
            scenes[i] = request.getParameter("idscene" + i);
        }

        for (int i = 0; i < nbScenes; i++) {
            sceneService.updateStatusScene(Integer.parseInt(scenes[i]), 1);
        }

        model.addAttribute("message", "Scenes confirmes !");
        model.addAttribute("liste", listeModifie);
        model.addAttribute("detailscene", detailSceneService);

        return "confirmee";
    }


    @GetMapping("/parametrer")
    public String parametre(){
        return "parametre";
    }

    @GetMapping("/attribuerheure")
    public String parametrer(Model model, HttpServletRequest request, HttpSession session){
        session.setAttribute("heuretravail", Integer.parseInt(request.getParameter("heuretravail")));
        model.addAttribute("message", "Heure de travail par jour parametree !");
        return "forward:/parametrer";
    }

    @GetMapping("/liste")
    public String afficherAFiltrer(Model model){
        listescene = sceneService.getAllScene();
        message = "";
        model.addAttribute("liste", listescene);
        model.addAttribute("message", message);
        return "recherche";
    }

    @GetMapping("/filtrer")
    public String filtrer(Model model, HttpServletRequest request){
        String[] spliter = request.getParameter("status").split("-");
        Integer status = Integer.parseInt(spliter[0]);
        listescene = sceneService.findScenesByStatus(status);
        message = spliter[1];
        model.addAttribute("message", message);
        model.addAttribute("liste", listescene);
        return "recherche";
    }

    @GetMapping("/supprimer")
    public String supprimerDansPlanning(HttpServletRequest request, Model model, HttpSession session){
        List<Jour> listeplanifie = (List<Jour>) session.getAttribute("listeplanifie");
        List<Scene> liste = null;
        for (int i = 0; i < listeplanifie.size(); i++) {
            liste = listeplanifie.get(i).getListScene();
            sceneService.supprimerScene(liste, Integer.parseInt(request.getParameter("id")));
            sceneService.supprimerJour(listeplanifie);
        }
        Date d1 = (Date) session.getAttribute("date1");
        Date d2 = (Date) session.getAttribute("date2");
        ArrayList<Date> listeDateValables = sceneService.listDateNotFerier(d1, d2);

        model.addAttribute("liste", listeplanifie);
        model.addAttribute("listeDate",listeDateValables);
        model.addAttribute("detailscene", detailSceneService);

        session.setAttribute("listeplanifie", listeplanifie);
        return "planning";
    }

    @GetMapping("/generer")
    public String genererPdf(Model model, HttpServletRequest request, HttpSession session) throws DocumentException, FileNotFoundException {
         List<Jour> listeModifie = (List<Jour>) session.getAttribute("listeplanifie");
         List<Integer> listeId = new ArrayList<>();
         String nompdf = request.getParameter("nompdf");
        for (int i = 0; i < listeModifie.size(); i++) {
            for (int j = 0; j < listeModifie.get(i).getListScene().size(); j++) {
                listeId.add(listeModifie.get(i).getListScene().get(j).getId());
            }
            sceneService.generer(nompdf, listeModifie, listeId);
        }
        model.addAttribute("pdf-ok","Exportation du planning en pdf avec succes!");
        return "periode";
    }
}
