package com.example.springmvc.model;

import javax.persistence.*;

@Entity
@Table(name="unite")
public class Unite {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name="nomunite")
    private String nomunite;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNomunite() {
        return nomunite;
    }

    public void setNomunite(String nomunite) {
        this.nomunite = nomunite;
    }
}
