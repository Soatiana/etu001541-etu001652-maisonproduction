package com.example.springmvc.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "personnage")
public class Personnage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "nompersonnage")
    private String nompersonnage;

    @Column(name="idgenre")
    private Integer idgenre;

    @Column(name = "datenaissance")
    private Date datenaissance;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNompersonnage() {
        return nompersonnage;
    }

    public void setNompersonnage(String nompersonnage) {
        this.nompersonnage = nompersonnage;
    }

    public Integer getIdgenre() {
        return idgenre;
    }

    public void setIdgenre(Integer idgenre) {
        this.idgenre = idgenre;
    }

    public Date getDatenaissance() {
        return datenaissance;
    }

    public void setDatenaissance(Date datenaissance) {
        this.datenaissance = datenaissance;
    }
}
