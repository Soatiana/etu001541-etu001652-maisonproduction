package com.example.springmvc.service;

import com.example.springmvc.dao.HibernateDAO;
import com.example.springmvc.model.Personnage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonnageService {
    @Autowired
    HibernateDAO hibernateDAO;

//    Get all personnage
    public List<Personnage> getAllPersonnage(){
        return hibernateDAO.getAll(Personnage.class);
    }
}
