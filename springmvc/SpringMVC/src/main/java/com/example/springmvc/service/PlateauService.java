package com.example.springmvc.service;

import com.example.springmvc.dao.HibernateDAO;
import com.example.springmvc.model.Plateau;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class PlateauService {
    @Autowired
    HibernateDAO hibernateDAO;

//    Get all plateau
    public List<Plateau> getAllPlateau(){
        return hibernateDAO.getAll(Plateau.class);
    }
//    Update status plateau
    public void updateEtatPlateau(Integer id, Integer etat) throws Exception {
        hibernateDAO.updateEtatPlateau(id, etat);
    }

    //    liste plateau where idScene n-checkena
    public List<Integer> listPlateauByIdScene(Integer idscene){
        return hibernateDAO.listPlateauByIdScene(idscene);
    }

    //    liste plateau rehetra amine liste idScene rehetra n-checkena
    public List<Integer> listeToutPlateau(List<Integer> listId) throws Exception {
        List<Integer> valiny = new ArrayList<>();
        List<Integer> listScene = null;
        for (int i = 0; i < listId.size(); i++) {
            listScene = this.listPlateauByIdScene(listId.get(i));
            for (int j = 0; j < listScene.size(); j++) {
                valiny.add(listScene.get(j));
            }
        }
        return valiny;
    }

//  liste idplateau tsy misy doublon intsony
    public List<Integer> plateauDistinct(List<Integer> listId) throws Exception {
        List<Integer> list = this.listeToutPlateau(listId);
        ArrayList<Integer> distinct = (ArrayList<Integer>) list.stream().distinct().collect(Collectors.toList());
        return distinct;
    }

//    Calcul distance
    public double calculDistance(double x, double y, double x1, double y1) {
        double deltaX = x1 - x;
        double deltaY = y1 - y;
        double distance = Math.sqrt(deltaX * deltaX + deltaY * deltaY);
        return distance;
    }

//    Liste plateau by id
    public List<Plateau> listPlateauById(Integer id){
        return hibernateDAO.findPlateauxById(id);
    }

//    liste plateau rehetra par rapport ane list idplateau retra n-checkena
    public List<Plateau> listePlateau(List<Integer> listId) throws Exception {
        List<Plateau> valiny = new ArrayList<>();
        List<Plateau> listPlateau = null;
        for (int i = 0; i < listId.size(); i++) {
            listPlateau = this.listPlateauById(listId.get(i));
            for (int j = 0; j < listPlateau.size(); j++) {
                valiny.add(listPlateau.get(j));
            }
        }

        return valiny;
    }

//    Trier plateaux par distance
    public List<Plateau> trierParDistance(List<Plateau> plateaux) {
        List<Plateau> sortedPlateaux = new ArrayList<>();
        if (plateaux != null && !plateaux.isEmpty()) {
            sortedPlateaux.add(plateaux.get(0));
            for (int i = 1; i < plateaux.size(); i++) {
                Plateau currentPlateau = plateaux.get(i);
                double currentDistance = calculDistance(currentPlateau.getX(), currentPlateau.getY(), sortedPlateaux.get(0).getX(), sortedPlateaux.get(0).getY());
                int indexToInsert = 0;
                for (int j = 1; j < sortedPlateaux.size(); j++) {
                    double distanceToPrevious = calculDistance(currentPlateau.getX(), currentPlateau.getY(), sortedPlateaux.get(j - 1).getX(), sortedPlateaux.get(j - 1).getY());
                    double distanceToNext = calculDistance(currentPlateau.getX(), currentPlateau.getY(), sortedPlateaux.get(j).getX(), sortedPlateaux.get(j).getY());
                    if (distanceToPrevious <= distanceToNext && distanceToPrevious <= currentDistance) {
                        currentDistance = distanceToPrevious;
                        indexToInsert = j;
                    } else if (distanceToNext < distanceToPrevious && distanceToNext <= currentDistance) {
                        currentDistance = distanceToNext;
                        indexToInsert = j;
                    }
                }
                sortedPlateaux.add(indexToInsert, currentPlateau);
            }
        }
        return sortedPlateaux;
    }
}
