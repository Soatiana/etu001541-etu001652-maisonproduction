package com.example.springmvc.service;

import com.example.springmvc.dao.HibernateDAO;
import com.example.springmvc.model.DetailScene;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DetailSceneService {
    @Autowired
    HibernateDAO hibernateDAO;

//    Save details scene
    public void save(DetailScene detailScene) throws Exception {
        hibernateDAO.save(detailScene);
    }

//    get detailscene where idscene = idscene
    public List<DetailScene> getAllBy(Integer idscene){
        return hibernateDAO.getAllBy(idscene);
    }

}
