package com.example.springmvc.service;

import com.example.springmvc.dao.HibernateDAO;
import com.example.springmvc.model.PlateauIndisponible;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PlateauIndisponibleService {
    @Autowired
    HibernateDAO hibernateDAO;

//    Save plateauIndisponible -> Changement etat plateau
    public void save(PlateauIndisponible plateauIndisponible) throws Exception {
        hibernateDAO.save(plateauIndisponible);
    }
}
