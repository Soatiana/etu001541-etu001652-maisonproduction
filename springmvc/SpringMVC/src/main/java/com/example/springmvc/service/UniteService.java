package com.example.springmvc.service;

import com.example.springmvc.dao.HibernateDAO;
import com.example.springmvc.model.Unite;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UniteService {
    @Autowired
    HibernateDAO hibernateDAO;

//    Get all unite (pour liste deroulante)
    public List<Unite> getAllUnite(){
        return hibernateDAO.getAll(Unite.class);
    }
}
