package com.example.springmvc.model;

import javax.persistence.*;

@Entity
@Table(name = "reaction")
public class Reaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name="nomreaction")
    private String nomreaction;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNomreaction() {
        return nomreaction;
    }

    public void setNomreaction(String nomreaction) {
        this.nomreaction = nomreaction;
    }
}
