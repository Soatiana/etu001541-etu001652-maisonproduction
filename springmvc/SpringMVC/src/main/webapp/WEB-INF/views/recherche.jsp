<%@ page import="com.example.springmvc.model.Plateau" %>
<%@ page import="java.util.List" %>
<%@ page import="com.example.springmvc.model.Scene" %>
<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <meta name="description" content="">
  <meta name="author" content="">

  <title>Filtre</title>

  <!-- CSS FILES -->
  <link rel="preconnect" href="https://fonts.googleapis.com">

  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>

  <link
          href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400&family=Sono:wght@200;300;400;500;700&display=swap"
          rel="stylesheet">

  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/bootstrap.min.css">

  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/bootstrap-icons.css">

  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/owl.carousel.min.css">

  <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/resources/css/owl.theme.default.min.css">

  <link href="${pageContext.request.contextPath}/resources/assets/css/templatemo-pod-talk.css" rel="stylesheet">
  <!--

TemplateMo 584 Pod Talk

https://templatemo.com/tm-584-pod-talk

-->
</head>

<body>

<main>

  <nav class="navbar navbar-expand-lg">
    <div class="container">
      <a class="navbar-brand me-lg-5 me-0" href="index.html">
        <span class="card-title" style="font-family: cursive">SK-Tournage</span>
      </a>


      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
              aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav ms-lg-auto">

          <li class="nav-item">
            <a class="nav-link" href="${pageContext.request.contextPath}/parametrer">Parametre</a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="${pageContext.request.contextPath}/periode">Plannification</a>
          </li>

          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarLightDropdownMenuLink" role="button"
               data-bs-toggle="dropdown" aria-expanded="false">Scene</a>

            <ul class="dropdown-menu dropdown-menu-light" aria-labelledby="navbarLightDropdownMenuLink">
              <li><a class="dropdown-item active" href="${pageContext.request.contextPath}/insertionscene">Insertion</a></li>
              <li><a class="dropdown-item active" href="${pageContext.request.contextPath}/liste">Liste</a></li>
            </ul>
          </li>

          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarLightDropdownMenuLink2" role="button"
               data-bs-toggle="dropdown" aria-expanded="false">Plateau</a>

            <ul class="dropdown-menu dropdown-menu-light" aria-labelledby="navbarLightDropdownMenuLink2">
              <li><a class="dropdown-item active" href="${pageContext.request.contextPath}/insertionplateau">Insertion</a></li>
              <li><a class="dropdown-item active" href="${pageContext.request.contextPath}/listeplateau">Liste</a></li>
              <li><a class="dropdown-item active" href="${pageContext.request.contextPath}//modifplateau">Modification status</a></li>
            </ul>
          </li>
        </ul>

      </div>
    </div>
  </nav>


  <header class="site-header d-flex flex-column justify-content-center align-items-center">
    <div class="container">
      <div class="row">

        <div class="col-lg-12 col-12 text-center">

          <h2 class="mb-0">Filtre</h2>
        </div>

      </div>
    </div>
  </header>
  <%
    List<Scene> liste = (List<Scene>) request.getAttribute("liste");
  %>
  <section class="contact-section section-padding pt-0">
    <div class="container">
      <div class="row">

        <div class="col-lg-8 col-12 mx-auto">
          <div class="section-title-wrap mb-5">
            <h4 class="section-title">Filtre scene par status scene</h4>
          </div>

          <form action="${pageContext.request.contextPath}/filtrer" method="get" class="custom-form contact-form" role="form">
            <div class="row">

              <div class="col-lg-6 col-md-6 col-12">
                <div class="form-floating">
                  <select name="status" class="form-select">
                    <option value="0-cree">Cree</option>
                    <option value="2-en cours">En cours</option>
                    <option value="3-ecriture terminee">Ecriture terminee</option>
                    <option value="1-planifie">Planifie</option>
                  </select>
                  <label for="floatingInput">Status scene</label>
                </div>
              </div>

              <div class="col-lg-4 col-12 ms-auto">
                <button type="submit" class="form-control">Filtrer                                                                                                                                                                                                                                                                     </button>
              </div>
              <%
                String message = (String) request.getAttribute("message");
              %>
              <div class="section-title-wrap mb-5">
                <h4 class="section-title">Liste scenes <%= message %></h4>
              </div>
              <% for(int i = 0; i < liste.size(); i++){ %>
              <div class="col-lg-6 col-12 mb-4 mb-lg-0">
                  <div class="custom-block d-flex">
                    <div class="custom-block-info">
                      <h5 class="mb-2">
                        <a href="detail-page.html">
                          <%= liste.get(i).getTitre() %>
                        </a>
                      </h5>
                      <div class="profile-block d-flex">
                        <p>
                          <strong>Duree : </strong><%= liste.get(i).getDuree() %> h
                        </p>
                      </div>
                      <div class="profile-block d-flex">
                        <p>
                          <strong>Plateau : </strong><%= liste.get(i).getPlateau().getNomplateau() %> <%= liste.get(i).getPlateau().getDescri() %>
                        </p>
                      </div>
                    </div>

                  </div>
              </div>
              <%  } %>
            </div>
        </div>

      </div>
    </div>
  </section>
</main>


<footer class="site-footer">
  <div class="container">
    <div class="row">

      <div class="col-lg-3 col-md-6 col-12 mb-4 mb-md-0 mb-lg-0">
        <h6 class="site-footer-title mb-3">Contact</h6>

        <p class="mb-2"><strong class="d-inline me-2">Phone:</strong> 010-020-0340</p>

        <p>
          <strong class="d-inline me-2">Email:</strong>
          <a href="#">inquiry@pod.co</a>
        </p>
      </div>

      <div class="col-lg-3 col-md-6 col-12">

        <h6 class="site-footer-title mb-3">Social</h6>

        <ul class="social-icon">
          <li class="social-icon-item">
            <a href="#" class="social-icon-link bi-instagram"></a>
          </li>

          <li class="social-icon-item">
            <a href="#" class="social-icon-link bi-twitter"></a>
          </li>

          <li class="social-icon-item">
            <a href="#" class="social-icon-link bi-whatsapp"></a>
          </li>
        </ul>
      </div>

    </div>
  </div>

  <div class="container pt-5">
    <div class="row align-items-center">
      <div class="col-lg-3 col-12">
        <p class="copyright-text mb-0">ETU001541-ETU001652
      </div>
    </div>
  </div>
</footer>

<!-- JAVASCRIPT FILES -->
<script src="${pageContext.request.contextPath}/resources/assets/js/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/assets/js/bootstrap.bundle.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/assets/js/owl.carousel.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/assets/js/custom.js"></script>

</body>

</html>